### Hi there 👋

<img src="https://raw.githubusercontent.com/MicaelliMedeiros/micaellimedeiros/master/image/computer-illustration.png" min-width="400px" max-width="400px" width="400px" align="right" alt="Computador iuriCode">

My name is Gabriel Damasceno 👨‍💻

I've been a Front-End developer since 2020. 💻

I am passionate about solving problems and learning. Always open to new challenges and ready to face changes.

### 🌍 "Rejeite o bom senso para tornar o impossível possível" 🧠

- 📍 From SP living in Osasco
-  💻 Front-End 

Reach me out 👇🏼

[![Youtube Badge](https://img.shields.io/badge/-Youtube-FF0000?style=flat-square&labelColor=FF0000&logo=youtube&logoColor=white&link=https://www.youtube.com/channel/UCRhKK6VrISnIWPJjYxBPKnA/videos)](https://www.youtube.com/channel/UCpiX8i2bfYBDEDxT-9QDlUQ) [![Instagram Badge](https://img.shields.io/badge/-Instagram-violet?style=flat-square&logo=Instagram&logoColor=white&link=https://www.instagram.com/papodedev/)](https://www.instagram.com/gabriellldamasceno) 

<div align="center">
  <a href="https://github.com/Gabriel-Damas">
    <img height="180em" src="https://github-readme-stats.vercel.app/api?username=Gabriel-Damas&count_private=true&include_all_commits=true&show_icons=true&theme=dracula&hide_border=false&show_owner=true"/>
    <img height="180em" src="https://github-readme-stats.vercel.app/api/top-langs/?username=Gabriel-Damas&theme=dracula&hide_border=false&&layout=compact"/>
  </a>
</div>

